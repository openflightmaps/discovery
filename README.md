# Discovery

This repository contains global settings for OFM.

To facilitate editing, each settings realm gets it's own YAML source file as described below. All realms are then regularly compiled into one discovery file and made available in different formats:

* https://snapshots.openflightmaps.org/discovery.json
* *XML not implemented yet - please ask*

Consumers must use the compiled files, not the source files!

## data/regions.yaml

This file contains all OFM regions along with some meta information. Each record has the following structure:

```yaml
LF:
  name: France
  country: FR
  firs:
    - LFBB
    - LFEE
    - LFFF
    - LFMM
    - LFRR
  contributors:
    - svoop
  status: preparing
```

* **OFM region identifier (primary key)**<br>By convention, the OFM region identifier is derived from the FIRs contained therein: In case there is exactly one FIR, the OFM region identifier inherits the 4 letter FIR identifier (e.g. `LSAS`). Otherwise, the OFM region identifier is equal to the first two letters of it's FIRs (e.g. `LF`).
* **Name**<br>English name of the OFM region. By convention, the mainland region of a political country takes just the country name: e.g. `France`. A non-mainland region of a political country takes both the name and a description: e.g. `France (Réunion)`. Oceanic regions take a corresponding description: e.g. `Portugal (Santa Maria Oceanic)`.
* **Country**<br>[2 letter ISO country code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
* **FIRs**<br>Array of FIRs contained in this OFM region
* **Contributors**<br>Array of GitLab usernames
* **Status**<br>Either `missing`, `preparing` or `released`

### Sources
* [OFMX region attribute](https://gitlab.com/openflightmaps/ofmx/-/wikis/Regions)
* [ICAO FIR list](https://www.icao.int/safety/FITS/Lists/Current%20FIR%20Status/FPL%20Status.aspx)
* [Wikipedia FIR list](https://en.wikipedia.org/wiki/Flight_information_region)
