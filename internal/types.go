package internal

import "time"

type Discovery struct {
	Version  string            `yaml:"version" json:"version" validate:"numeric"`
	Metadata Metadata          `yaml:"metadata" json:"metadata" validate:"dive"`
	Regions  map[string]Region `yaml:"regions" json:"regions" validate:"dive"`
}

type Metadata struct {
	LastUpdate time.Time `yaml:"last_update" json:"last_update"`
}

type Region struct {
	Firs    []string `yaml:"firs" json:"firs" validate:"dive,alpha"`
	Country string   `yaml:"country" json:"country" validate:"alpha"`
	Status  string   `yaml:"status" json:"status" validate:"oneof=missing preparing released"`
}
