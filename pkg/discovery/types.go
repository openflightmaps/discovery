package discovery

const statusReleased = "released"
const statusPreparing = "preparing"

type Region struct {
	Firs    []string
	Country string
	Status  string
}
