package discovery

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"

	"gitlab.com/openflightmaps/discovery/internal"
)

const DISCO_URL_PROD = "https://snapshots.openflightmaps.org/discovery.json"
const DISCO_URL_DEV = "https://snapshots-dev.openflightmaps.org/discovery.json"

type Discovery struct {
	url        string
	unreleased bool
	discovery  *internal.Discovery
}

type Option func(d *Discovery)

func Dev() Option {
	return func(d *Discovery) {
		d.url = DISCO_URL_DEV
	}
}

func Unreleased() Option {
	return func(d *Discovery) {
		d.unreleased = true
	}
}

func (d *Discovery) discover() (*internal.Discovery, error) {
	resp, err := http.Get(d.url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	disco := &internal.Discovery{}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(disco)
	return disco, err
}

func (d *Discovery) Regions() []string {
	r := make([]string, 0)
	for k, v := range d.discovery.Regions {
		switch v.Status {
		case statusReleased:
			r = append(r, k)
		case statusPreparing:
			if d.unreleased {
				r = append(r, k)
			}
		}
	}
	sort.StringSlice(r).Sort()
	return r
}

func (d *Discovery) Region(name string) (*Region, error) {
	r, ok := d.discovery.Regions[name]
	if !ok {
		return nil, fmt.Errorf("Invalid region name %s", name)
	}
	reg := Region{Firs: r.Firs, Country: r.Country, Status: r.Status}
	return &reg, nil
}

func NewDiscovery(opts ...Option) (*Discovery, error) {
	var err error
	d := &Discovery{url: DISCO_URL_PROD}
	for _, opt := range opts {
		opt(d)
	}
	d.discovery, err = d.discover()
	return d, err
}
