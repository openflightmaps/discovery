package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"gitlab.com/openflightmaps/discovery/internal"
	"gopkg.in/go-playground/validator.v9"
	en_translations "gopkg.in/go-playground/validator.v9/translations/en"
	"gopkg.in/yaml.v2"
)

func main() {
	fr, err := os.Open("data/regions.yaml")
	if err != nil {
		panic(err)
	}
	defer fr.Close()

	d := internal.Discovery{}
	err = yaml.NewDecoder(fr).Decode(&d)
	if err != nil {
		panic(err)
	}

	d.Version = "1"
	d.Metadata.LastUpdate = time.Now()

	validate := validator.New()
	en := en.New()
	uni := ut.New(en, en)
	trans, _ := uni.GetTranslator("en")
	en_translations.RegisterDefaultTranslations(validate, trans)

	err = validate.Struct(d)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			panic(err)
		}
		for _, err := range err.(validator.ValidationErrors) {
			fmt.Printf("%s: Actual Value: '%s'\n", err.Translate(trans), err.Value())
		}
		panic("Input file invalid")
	}

	os.Mkdir("output", os.ModePerm)
	foj, err := os.Create("output/discovery.json")
	if err != nil {
		panic(err)
	}
	defer foj.Close()
	err = json.NewEncoder(foj).Encode(d)
	if err != nil {
		panic(err)
	}
}
