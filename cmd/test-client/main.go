package main

import (
	"fmt"

	"gitlab.com/openflightmaps/discovery/pkg/discovery"
)

func main() {
	d, err := discovery.NewDiscovery(discovery.Unreleased())
	if err != nil {
		panic(err)
	}
	regions := d.Regions()
	fmt.Println(regions)
	for _, rn := range regions {
		r, err := d.Region(rn)
		if err != nil {
			panic(err)
		}
		fmt.Println(r)
	}
}
